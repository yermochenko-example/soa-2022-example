INSERT INTO "client"
------------------------------------------------------------------------------------------------------------------
("id", "name"                                   , "address"                        , "phone"              ) VALUES
------------------------------------------------------------------------------------------------------------------
(1   , 'Нотариальная контора «Иванов и сыновья»', 'ул. Ленина, 123, оф. 45'        , '+375 (29) 123-45-67'),
(2   , 'Копицентр «Петров & Co»'                , 'пр-т Победы, 67, оф. 890'       , '+375 (33) 234-56-78'),
(3   , 'Архитектурное агентство «Ральф»'        , 'ул. Мира, 12'                   , '+375 (29) 345-67-89'),
(4   , 'Рекламное агентство «Братья Сидоровы»'  , 'пер. Коммунистический, 34-5, 67', '+375 (44) 456-78-90'),
(5   , 'Телеканал «Местные новости»'            , 'ул. Дзержинского, 89'           , '+375 (29) 567-89-01');
------------------------------------------------------------------------------------------------------------------
SELECT setval('client_id_seq', 5);

INSERT INTO "order"
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
("id", "client_id", "amount", "price" , "status_id", "date_open" , "date_close", "hardware"                                   , "software"                                                                                             ) VALUES
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(1   , 1          , 3       , 834000  , 3          , '2022-02-01', '2022-02-01', 'Скиф 987, CPU 2×3.1, RAM 8 Gb, HDD 2 Tb'    , 'Adobe Acrobat Reader, Google Chrome, Microsoft Office'                                                ),
(2   , 2          , 2       , 804526  , 2          , '2022-02-01', NULL        , 'HP 1234, CPU 4×2.5, RAM 16 Gb, HDD 2 Tb'    , 'Adobe Acrobat Reader, Adobe Photoshop, Corel Draw, Google Chrome, Microsoft Office'                   ),
(3   , 4          , 5       , 2011315 , 6          , '2022-02-01', '2022-02-11', 'HP 1234, CPU 4×2.5, RAM 16 Gb, HDD 2 Tb'    , 'Adobe Acrobat Reader, Adobe Photoshop, Corel Draw, Google Chrome, Microsoft Office'                   ),
(4   , 3          , 4       , 1112000 , 6          , '2022-02-01', '2022-02-10', 'Скиф 987, CPU 2×3.1, RAM 8 Gb, HDD 2 Tb'    , 'Adobe Acrobat Reader, Google Chrome, Microsoft Office'                                                ),
(5   , 4          , 3       , 1554288 , 5          , '2022-02-03', '2022-02-18', 'Compaq 7-89, CPU 4×2.8, RAM 32 Gb, HDD 3 Tb', '3Ds Max, Adobe Acrobat Reader, Auto CAD, Google Chrome, Microsoft Office'                             ),
(6   , 4          , 10      , 2780000 , 6          , '2022-02-07', '2022-02-11', 'Скиф 987, CPU 2×3.1, RAM 8 Gb, HDD 2 Tb'    , 'Adobe Acrobat Reader, Google Chrome, Microsoft Office'                                                ),
(7   , 3          , 10      , 6013160 , 4          , '2022-02-07', NULL        , 'Intel 56, CPU 6×2.2, RAM 32 Gb, HDD 4 Tb'   , '3Ds Max, Adobe Acrobat Reader, Adobe Photoshop, Auto CAD, Corel Draw, Google Chrome, Microsoft Office'),
(8   , 5          , 14      , 3892000 , 4          , '2022-02-07', NULL        , 'Скиф 987, CPU 2×3.1, RAM 8 Gb, HDD 2 Tb'    , 'Adobe Acrobat Reader, Google Chrome, Microsoft Office'                                                ),
(9   , 5          , 6       , 3607896 , 2          , '2022-02-09', NULL        , 'Intel 56, CPU 6×2.2, RAM 32 Gb, HDD 4 Tb'   , '3Ds Max, Adobe Acrobat Reader, Adobe Photoshop, Auto CAD, Corel Draw, Google Chrome, Microsoft Office'),
(10  , 4          , 2       , 1468000 , 1          , '2022-02-09', NULL        , 'Скиф 012, CPU 8×2.4, RAM 64 Gb, HDD 4 Tb'   , 'Adobe Acrobat Reader, Adobe Premiere Pro, Google Chrome, Microsoft Office'                            ),
(11  , 5          , 12      , 11190876, 1          , '2022-02-11', NULL        , 'Compaq 3-45, CPU 8×2.9, RAM 64 Gb, HDD 6 Tb', '3Ds Max, Adobe Acrobat Reader, Adobe Premiere Pro, Auto CAD, Google Chrome, Microsoft Office'         );
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT setval('order_id_seq', 11);

INSERT INTO "order_scopes"
-------------------------------
("order_id", "scope_id") VALUES
-------------------------------
(1         , 1         ),
(1         , 5         ),
(2         , 1         ),
(2         , 2         ),
(2         , 5         ),
(3         , 1         ),
(3         , 2         ),
(3         , 5         ),
(4         , 1         ),
(4         , 5         ),
(5         , 1         ),
(5         , 3         ),
(5         , 5         ),
(6         , 1         ),
(6         , 5         ),
(7         , 1         ),
(7         , 2         ),
(7         , 3         ),
(7         , 5         ),
(8         , 1         ),
(8         , 5         ),
(9         , 1         ),
(9         , 2         ),
(9         , 3         ),
(9         , 5         ),
(10        , 1         ),
(10        , 4         ),
(10        , 5         ),
(11        , 1         ),
(11        , 3         ),
(11        , 4         ),
(11        , 5         );
