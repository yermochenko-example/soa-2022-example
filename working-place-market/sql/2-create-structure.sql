DROP TABLE IF EXISTS "order_scopes";
DROP TABLE IF EXISTS "order";
DROP TABLE IF EXISTS "client";
DROP TABLE IF EXISTS "status";
DROP TABLE IF EXISTS "scope";

CREATE TABLE "scope" (
    "id"          SERIAL  PRIMARY KEY,
    "name"        TEXT    NOT NULL
);

CREATE TABLE "status" (
    "id"         SERIAL  PRIMARY KEY,
    "name"       TEXT    NOT NULL,
    "active"     BOOLEAN NOT NULL
);

CREATE TABLE "client" (
    "id"         SERIAL  PRIMARY KEY,
    "name"       TEXT    NOT NULL,
    "address"    TEXT    NOT NULL,
    "phone"      TEXT    NOT NULL
);

CREATE TABLE "order" (
    "id"         SERIAL  PRIMARY KEY,
    "client_id"  INTEGER NOT NULL REFERENCES "client" ON UPDATE RESTRICT ON DELETE RESTRICT,
    "hardware"   TEXT    NOT NULL,
    "software"   TEXT    NOT NULL,
    "amount"     INTEGER NOT NULL,
    "price"      INTEGER NOT NULL,
    "status_id"  INTEGER NOT NULL REFERENCES "status" ON UPDATE RESTRICT ON DELETE RESTRICT,
    "date_open"  DATE    NOT NULL,
    "date_close" DATE
);

CREATE TABLE "order_scopes" (
    "order_id"   INTEGER NOT NULL REFERENCES "order" ON UPDATE RESTRICT ON DELETE CASCADE,
    "scope_id"   INTEGER NOT NULL REFERENCES "scope" ON UPDATE RESTRICT ON DELETE RESTRICT,
    PRIMARY KEY ("order_id", "scope_id")
);
