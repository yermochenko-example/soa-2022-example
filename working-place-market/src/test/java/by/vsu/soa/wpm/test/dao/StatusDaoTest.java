package by.vsu.soa.wpm.test.dao;

import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.dao.StatusDao;
import by.vsu.soa.wpm.dao.db.StatusDaoPostgreSqlImpl;
import by.vsu.soa.wpm.domain.Status;

import java.sql.Connection;
import java.sql.SQLException;

public class StatusDaoTest {
	private static void printStatuses(StatusDao statusDao) throws DaoException {
		var statuses = statusDao.readAll();
		System.out.println("StatusDao.readAll() try to call...");
		System.out.println("+------+------------+--------+");
		System.out.println("|  id  |    name    | active |");
		System.out.println("+------+------------+--------+");
		for(var status : statuses) {
			System.out.printf("| %04d | %-10s | %-6s |\n", status.getId(), status.getName(), status.isActive());
		}
		System.out.println("+------+------------+--------+");
		System.out.println("StatusDao.readAll() called OK\n");
	}

	public static void main(String[] args) {
		try(Connection c = DatabaseConnector.getConnection()) {
			var statusDao = new StatusDaoPostgreSqlImpl();
			statusDao.setConnection(c);
			printStatuses(statusDao);
			System.out.println("StatusDao.create() try to call...");
			var status = new Status();
			status.setName("Тест");
			status.setActive(true);
			var id = statusDao.create(status);
			status.setId(id);
			System.out.printf("    ID = %d\n", id);
			System.out.println("StatusDao.create() called OK\n");
			printStatuses(statusDao);
			System.out.println("StatusDao.read() try to call...");
			var readStatus = statusDao.read(id);
			System.out.printf("    id    : %d\n", readStatus.getId());
			System.out.printf("    name  : %s\n", readStatus.getName());
			System.out.printf("    active: %s\n", readStatus.isActive());
			System.out.println("StatusDao.read() called OK\n");
			printStatuses(statusDao);
			System.out.println("StatusDao.update() try to call...");
			status.setName("Test (UPD)");
			status.setActive(false);
			statusDao.update(status);
			System.out.println("StatusDao.update() called OK\n");
			printStatuses(statusDao);
			System.out.println("StatusDao.delete() try to call...");
			statusDao.delete(id);
			System.out.println("StatusDao.delete() called OK\n");
			printStatuses(statusDao);
		} catch(DaoException | SQLException e) {
			e.printStackTrace();
		}
	}
}
