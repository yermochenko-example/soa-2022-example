package by.vsu.soa.wpm.test.dao;

import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.dao.ScopeDao;
import by.vsu.soa.wpm.dao.db.ScopeDaoPostgreSqlImpl;
import by.vsu.soa.wpm.domain.Scope;

import java.sql.Connection;
import java.sql.SQLException;

public class ScopeDaoTest {
	private static void printScopes(ScopeDao scopeDao) throws DaoException {
		var scopes = scopeDao.readAll();
		System.out.println("ScopeDao.readAll() try to call...");
		System.out.println("+------+----------------------+");
		System.out.println("|  id  |         name         |");
		System.out.println("+------+----------------------+");
		for(var scope : scopes) {
			System.out.printf("| %04d | %-20s |\n", scope.getId(), scope.getName());
		}
		System.out.println("+------+----------------------+");
		System.out.println("ScopeDao.readAll() called OK\n");
	}

	public static void main(String[] args) {
		try(Connection c = DatabaseConnector.getConnection()) {
			var scopeDao = new ScopeDaoPostgreSqlImpl();
			scopeDao.setConnection(c);
			printScopes(scopeDao);
			System.out.println("ScopeDao.create() try to call...");
			var scope = new Scope();
			scope.setName("Тестовый scope");
			var id = scopeDao.create(scope);
			scope.setId(id);
			System.out.printf("    ID = %d\n", id);
			System.out.println("ScopeDao.create() called OK\n");
			printScopes(scopeDao);
			System.out.println("ScopeDao.read() try to call...");
			var readScope = scopeDao.read(id);
			System.out.printf("    id  : %d\n", readScope.getId());
			System.out.printf("    name: %s\n", readScope.getName());
			System.out.println("ScopeDao.read() called OK\n");
			printScopes(scopeDao);
			System.out.println("ScopeDao.update() try to call...");
			scope.setName("Тестовый scope (UPD)");
			scopeDao.update(scope);
			System.out.println("ScopeDao.update() called OK\n");
			printScopes(scopeDao);
			System.out.println("ScopeDao.delete() try to call...");
			scopeDao.delete(id);
			System.out.println("ScopeDao.delete() called OK\n");
			printScopes(scopeDao);
		} catch(DaoException | SQLException e) {
			e.printStackTrace();
		}
	}
}
