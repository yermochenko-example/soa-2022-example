package by.vsu.soa.wpm.test.dao;

import by.vsu.soa.wpm.dao.ClientDao;
import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.dao.db.ClientDaoPostgreSqlImpl;
import by.vsu.soa.wpm.domain.Client;

import java.sql.Connection;
import java.sql.SQLException;

public class ClientDaoTest {
	private static void printClients(ClientDao clientDao) throws DaoException {
		var clients = clientDao.readAll();
		System.out.println("ClientDao.readAll() try to call...");
		System.out.println("+------+------------------------------------------+-------------------------------------+---------------------------+");
		System.out.println("|  id  |                   name                   |               address               |           phone           |");
		System.out.println("+------+------------------------------------------+-------------------------------------+---------------------------+");
		for(var client : clients) {
			System.out.printf("| %04d | %-40s | %-35s | %-25s |\n", client.getId(), client.getName(), client.getAddress(), client.getPhone());
		}
		System.out.println("+------+------------------------------------------+-------------------------------------+---------------------------+");
		System.out.println("ClientDao.readAll() called OK\n");
	}

	public static void main(String[] args) {
		try(Connection c = DatabaseConnector.getConnection()) {
			var clientDao = new ClientDaoPostgreSqlImpl();
			clientDao.setConnection(c);
			printClients(clientDao);
			System.out.println("ClientDao.create() try to call...");
			var client = new Client();
			client.setName("Тестовый клиент");
			client.setPhone("+375 (212) 12-34-56");
			client.setAddress("Тут");
			var id = clientDao.create(client);
			client.setId(id);
			System.out.printf("    ID = %d\n", id);
			System.out.println("ClientDao.create() called OK\n");
			printClients(clientDao);
			System.out.println("ClientDao.read() try to call...");
			var readClient = clientDao.read(id);
			System.out.printf("    id     : %d\n", readClient.getId());
			System.out.printf("    name   : %s\n", readClient.getName());
			System.out.printf("    address: %s\n", readClient.getAddress());
			System.out.printf("    phone  : %s\n", readClient.getPhone());
			System.out.println("ClientDao.read() called OK\n");
			printClients(clientDao);
			System.out.println("ClientDao.update() try to call...");
			client.setName("Тестовый клиент (UPD)");
			client.setPhone("+375 (212) 98-76-54");
			client.setAddress("Там");
			clientDao.update(client);
			System.out.println("ClientDao.update() called OK\n");
			printClients(clientDao);
			System.out.println("ClientDao.delete() try to call...");
			clientDao.delete(id);
			System.out.println("ClientDao.delete() called OK\n");
			printClients(clientDao);
		} catch(DaoException | SQLException e) {
			e.printStackTrace();
		}
	}
}
