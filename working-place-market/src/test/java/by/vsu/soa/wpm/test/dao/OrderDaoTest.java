package by.vsu.soa.wpm.test.dao;

import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.dao.OrderDao;
import by.vsu.soa.wpm.dao.db.OrderDaoPostgreSqlImpl;
import by.vsu.soa.wpm.domain.Client;
import by.vsu.soa.wpm.domain.Order;
import by.vsu.soa.wpm.domain.Scope;
import by.vsu.soa.wpm.domain.Status;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.stream.Collectors;

public class OrderDaoTest {
	public static final Long CLIENT_ID = 4L;

	private static void printOrders(OrderDao orderDao) throws DaoException {
		var orders = orderDao.readByClient(CLIENT_ID);
		var dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
		System.out.println("OrderDao.readByClient() try to call...");
		System.out.println("+------+--------+--------+-----------------------+--------+------------+------------+--------------------------------+----------------------------------------------------+----------------------------------------------------------------------------------------------------------------+");
		System.out.println("|  id  | client | amount |         price         | status | date_open  | date_close |             scopes             |                      hardware                      |                                                    software                                                    |");
		System.out.println("+------+--------+--------+-----------------------+--------+------------+------------+--------------------------------+----------------------------------------------------+----------------------------------------------------------------------------------------------------------------+");
		for(var order : orders) {
			var dateOpen = dateFormatter.format(order.getDateOpen());
			var dateClose = order.getDateClose() != null ? dateFormatter.format(order.getDateClose()) : "NULL";
			var scopes = order.getScopes() != null && !order.getScopes().isEmpty() ? order.getScopes().stream().map(scope -> String.format("%04d", scope.getId())).collect(Collectors.joining(", ")) : "NULL";
			System.out.printf("| %04d |  %04d  |   %2d   | %,8d руб. %02d коп. |  %04d  | %-10s | %-10s | %-30s | %-50s | %-110s |\n", order.getId(), order.getClient().getId(), order.getAmount(), order.getPrice() / 100, order.getPrice() % 100, order.getStatus().getId(), dateOpen, dateClose, scopes, order.getHardware(), order.getSoftware());
		}
		System.out.println("+------+--------+--------+-----------------------+--------+------------+------------+--------------------------------+----------------------------------------------------+----------------------------------------------------------------------------------------------------------------+");
		System.out.println("OrderDao.readByClient() called OK\n");
	}

	public static void main(String[] args) {
		var scopes = new Scope[] {new Scope(), new Scope(), new Scope(), new Scope(), new Scope()};
		for(int i = 0; i < scopes.length; i++) {
			scopes[i].setId(i + 1L);
		}
		try(Connection c = DatabaseConnector.getConnection()) {
			var orderDao = new OrderDaoPostgreSqlImpl();
			orderDao.setConnection(c);
			printOrders(orderDao);
			System.out.println("OrderDao.create() try to call...");
			var order1 = new Order();
			order1.setClient(new Client());
			order1.getClient().setId(CLIENT_ID);
			order1.setAmount(1);
			order1.setPrice(1659511);
			order1.setStatus(new Status());
			order1.getStatus().setId(2L);
			var calendar = Calendar.getInstance();
			calendar.set(2022, Calendar.FEBRUARY, 20);
			order1.setDateOpen(calendar.getTime());
			order1.setHardware("Dell 2357, CPU 8×2.5, RAM 16 Gb, HDD 1 Tb");
			order1.setSoftware("Ubuntu Linux");
			var id1 = orderDao.create(order1);
			order1.setId(id1);
			System.out.printf("    ID = %d\n", id1);
			var order2 = new Order();
			order2.setClient(new Client());
			order2.getClient().setId(CLIENT_ID);
			order2.setAmount(1);
			order2.setPrice(1659511);
			order2.setStatus(new Status());
			order2.getStatus().setId(1L);
			calendar.set(2022, Calendar.FEBRUARY, 22);
			order2.setDateOpen(calendar.getTime());
			order2.setScopes(new HashSet<>());
			order2.getScopes().add(scopes[0]);
			order2.getScopes().add(scopes[2]);
			order2.getScopes().add(scopes[4]);
			order2.setHardware("Dell 2357, CPU 8×2.5, RAM 16 Gb, HDD 1 Tb");
			order2.setSoftware("Windows 10");
			var id2 = orderDao.create(order2);
			order2.setId(id2);
			System.out.printf("    ID = %d\n", id2);
			System.out.println("OrderDao.create() called OK\n");
			printOrders(orderDao);
			System.out.println("OrderDao.read() try to call...");
			var dateFormatter = new SimpleDateFormat("d MMMM yyyy");
			var readOrder1 = orderDao.read(id1);
			var dateOpen1 = dateFormatter.format(readOrder1.getDateOpen());
			var dateClose1 = readOrder1.getDateClose() != null ? dateFormatter.format(readOrder1.getDateClose()) : "заказ не закрыт";
			var scopes1 = readOrder1.getScopes() != null && !readOrder1.getScopes().isEmpty() ? readOrder1.getScopes().stream().map(scope -> String.format("%d", scope.getId())).collect(Collectors.joining(", ")) : "scopes не заданы";
			System.out.printf("    id        : %d\n", readOrder1.getId());
			System.out.printf("    client id : %d\n", readOrder1.getClient().getId());
			System.out.printf("    amount    : %d\n", readOrder1.getAmount());
			System.out.printf("    price     : %,d руб. %02d коп.\n", readOrder1.getPrice() / 100, readOrder1.getPrice() % 100);
			System.out.printf("    status id : %d\n", readOrder1.getStatus().getId());
			System.out.printf("    date open : %s\n", dateOpen1);
			System.out.printf("    date close: %s\n", dateClose1);
			System.out.printf("    scopes ids: %s\n", scopes1);
			System.out.printf("    hardware  : %s\n", readOrder1.getHardware());
			System.out.printf("    software  : %s\n", readOrder1.getSoftware());
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
			var readOrder2 = orderDao.read(id2);
			var dateOpen2 = dateFormatter.format(readOrder2.getDateOpen());
			var dateClose2 = readOrder2.getDateClose() != null ? dateFormatter.format(readOrder2.getDateClose()) : "заказ не закрыт";
			var scopes2 = readOrder2.getScopes() != null && !readOrder2.getScopes().isEmpty() ? readOrder2.getScopes().stream().map(scope -> String.format("%d", scope.getId())).collect(Collectors.joining(", ")) : "scopes не заданы";
			System.out.printf("    id        : %d\n", readOrder2.getId());
			System.out.printf("    client id : %d\n", readOrder2.getClient().getId());
			System.out.printf("    amount    : %d\n", readOrder2.getAmount());
			System.out.printf("    price     : %,d руб. %02d коп.\n", readOrder2.getPrice() / 100, readOrder2.getPrice() % 100);
			System.out.printf("    status id : %d\n", readOrder2.getStatus().getId());
			System.out.printf("    date open : %s\n", dateOpen2);
			System.out.printf("    date close: %s\n", dateClose2);
			System.out.printf("    scopes ids: %s\n", scopes2);
			System.out.printf("    hardware  : %s\n", readOrder2.getHardware());
			System.out.printf("    software  : %s\n", readOrder2.getSoftware());
			System.out.println("OrderDao.read() called OK\n");
			printOrders(orderDao);
			System.out.println("OrderDao.update() try to call...");
			order1.setAmount(3);
			order1.setPrice(4480679);
			order1.getStatus().setId(6L);
			calendar.set(2022, Calendar.FEBRUARY, 22);
			order1.setDateClose(calendar.getTime());
			order1.setScopes(new HashSet<>());
			order1.getScopes().add(scopes[1]);
			order1.getScopes().add(scopes[2]);
			order1.setSoftware("Debian Linux");
			orderDao.update(order1);
			order2.setAmount(5);
			order2.setPrice(7052921);
			order2.getStatus().setId(2L);
			order2.setScopes(new HashSet<>());
			order2.getScopes().add(scopes[1]);
			order2.getScopes().add(scopes[2]);
			order2.getScopes().add(scopes[3]);
			order2.setSoftware("Windows 11");
			orderDao.update(order2);
			System.out.println("OrderDao.update() called OK\n");
			printOrders(orderDao);
			System.out.println("OrderDao.delete() try to call...");
			orderDao.delete(id1);
			orderDao.delete(id2);
			System.out.println("OrderDao.delete() called OK\n");
			printOrders(orderDao);
		} catch(DaoException | SQLException e) {
			e.printStackTrace();
		}
	}
}
