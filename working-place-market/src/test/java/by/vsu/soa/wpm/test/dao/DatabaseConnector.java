package by.vsu.soa.wpm.test.dao;

import by.vsu.soa.wpm.dao.DaoException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {
	public static final String JDBC_DRIVER_CLASS = "org.postgresql.Driver";
	public static final String JDBC_URL = "jdbc:postgresql://localhost:5432/soa_2022_working_place_market_db";
	public static final String JDBC_USER = "root";
	public static final String JDBC_PASSWORD = "root";

	private static boolean driverLoaded;

	public static Connection getConnection() throws DaoException {
		try {
			if(!driverLoaded) {
				Class.forName(JDBC_DRIVER_CLASS);
				driverLoaded = true;
			}
			return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
		} catch(ClassNotFoundException | SQLException e) {
			throw new DaoException(e);
		}
	}
}
