package by.vsu.soa.wpm.service.main;

import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.dao.ScopeDao;
import by.vsu.soa.wpm.domain.Scope;
import by.vsu.soa.wpm.service.ScopeService;
import by.vsu.soa.wpm.service.ServiceException;

import java.util.List;

public class ScopeServiceImpl implements ScopeService {
	private ScopeDao scopeDao;

	public void setScopeDao(ScopeDao scopeDao) {
		this.scopeDao = scopeDao;
	}

	@Override
	public List<Scope> findAll() throws ServiceException {
		try {
			return scopeDao.readAll();
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Scope findById(Long id) throws ServiceException {
		try {
			return scopeDao.read(id);
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void create(Scope scope) throws ServiceException {
		try {
			var id = scopeDao.create(scope);
			scope.setId(id);
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(Scope scope) throws ServiceException {
		try {
			scopeDao.update(scope);
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			scopeDao.delete(id);
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}
}
