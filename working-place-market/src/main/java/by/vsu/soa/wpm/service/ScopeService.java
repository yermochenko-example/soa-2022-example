package by.vsu.soa.wpm.service;

import by.vsu.soa.wpm.domain.Scope;

import java.util.List;

public interface ScopeService {
	List<Scope> findAll() throws ServiceException;

	Scope findById(Long id) throws ServiceException;

	void create(Scope scope) throws ServiceException;

	void update(Scope scope) throws ServiceException;

	void delete(Long id) throws ServiceException;
}
