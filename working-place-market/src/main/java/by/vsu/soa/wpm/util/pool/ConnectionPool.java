package by.vsu.soa.wpm.util.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConnectionPool {
	private static final ConnectionPool instance = new ConnectionPool();
	private static final ExecutorService closer = Executors.newSingleThreadExecutor();

	private String jdbcUrl;
	private String jdbcUser;
	private String jdbcPassword;
	private int maxSize;
	private int validationTimeout;

	private final Queue<Connection> freeConnections = new ConcurrentLinkedDeque<>();
	private final Set<Connection> usedConnections = new ConcurrentSkipListSet<>(Comparator.comparingInt(Object::hashCode));

	private ConnectionPool() {}

	public static ConnectionPool getInstance() {
		return instance;
	}

	public Connection getConnection() throws PoolException {
		Connection connection = null;
		while(connection == null) {
			try {
				connection = freeConnections.poll();
				if(connection != null) {
					if(!connection.isValid(validationTimeout)) {
						close(connection);
						connection = null;
					}
				} else if(maxSize == 0 || usedConnections.size() < maxSize) {
					connection = establishConnection();
				} else {
					throw new PoolException("Pool max size exceeded");
				}
			} catch(SQLException e) {
				throw new PoolException(e);
			}
		}
		usedConnections.add(connection);
		return new ConnectionWrapper(connection);
	}

	public void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword, int minSize, int maxSize, int validationTimeout) throws PoolException {
		try {
			if(minSize <= maxSize) {
				Class.forName(jdbcDriver);
				this.jdbcUrl = jdbcUrl;
				this.jdbcUser = jdbcUser;
				this.jdbcPassword = jdbcPassword;
				this.maxSize = maxSize;
				this.validationTimeout = validationTimeout;
				for(var i = 0; i < minSize; i++) {
					freeConnections.add(establishConnection());
				}
			} else {
				throw new PoolException("Incorrect Pool initialization. Parameter minSize is to be less or equal parameter maxSize");
			}
		} catch(ClassNotFoundException | SQLException e) {
			throw new PoolException(e);
		}
	}

	public void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword, int minSize, int maxSize) throws PoolException {
		init(jdbcDriver, jdbcUrl, jdbcUser, jdbcPassword, minSize, maxSize, 0);
	}

	public void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword, int maxSize) throws PoolException {
		init(jdbcDriver, jdbcUrl, jdbcUser, jdbcPassword, 0, maxSize);
	}

	public void init(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword) throws PoolException {
		init(jdbcDriver, jdbcUrl, jdbcUser, jdbcPassword, 0);
	}

	public void destroy() {
		synchronized(usedConnections) {
			synchronized(freeConnections) {
				usedConnections.addAll(freeConnections);
				freeConnections.clear();
				for(var connection : usedConnections) {
					close(connection);
				}
				usedConnections.clear();
				closer.shutdown();
			}
		}
	}

	void freeConnection(Connection connection) throws SQLException {
		try {
			usedConnections.remove(connection);
			connection.clearWarnings();
			connection.setAutoCommit(true);
			freeConnections.add(connection);
		} catch(SQLException e) {
			close(connection);
			throw e;
		}
	}

	private Connection establishConnection() throws SQLException {
		return DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
	}

	private void close(Connection connection) {
		closer.execute(() -> {
			synchronized(connection) {
				try { connection.rollback(); } catch(SQLException e) { e.printStackTrace(); }
				try { connection.close(); } catch(SQLException e) { e.printStackTrace(); }
			}
		});
	}
}
