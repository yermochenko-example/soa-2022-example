package by.vsu.soa.wpm.service;

public class ServiceException extends Exception {
	public ServiceException(Throwable cause) {
		super(cause);
	}
}
