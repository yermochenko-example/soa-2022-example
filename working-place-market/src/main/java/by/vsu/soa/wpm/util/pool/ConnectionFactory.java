package by.vsu.soa.wpm.util.pool;

import by.vsu.soa.wpm.util.ioc.Factory;
import by.vsu.soa.wpm.util.ioc.IoCException;

import java.sql.Connection;

public class ConnectionFactory implements Factory<Connection> {
	@Override
	public Connection newInstance() throws IoCException {
		try {
			return ConnectionPool.getInstance().getConnection();
		} catch(PoolException e) {
			throw new IoCException(e);
		}
	}
}
