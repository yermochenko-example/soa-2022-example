package by.vsu.soa.wpm.util.ioc;

public class IoCException extends Exception {
	public IoCException(Throwable cause) {
		super(cause);
	}
}
