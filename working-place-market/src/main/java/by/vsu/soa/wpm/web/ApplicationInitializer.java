package by.vsu.soa.wpm.web;

import by.vsu.soa.wpm.dao.ScopeDao;
import by.vsu.soa.wpm.dao.db.ScopeDaoPostgreSqlImpl;
import by.vsu.soa.wpm.service.ScopeService;
import by.vsu.soa.wpm.service.main.ScopeServiceImpl;
import by.vsu.soa.wpm.util.ioc.IoCContainer;
import by.vsu.soa.wpm.util.ioc.IoCException;
import by.vsu.soa.wpm.util.pool.ConnectionFactory;
import by.vsu.soa.wpm.util.pool.ConnectionPool;
import by.vsu.soa.wpm.util.pool.PoolException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Connection;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class ApplicationInitializer implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			// Setting up a connection pool
			var context = event.getServletContext();
			var jdbcDriver   = context.getInitParameter("jdbc-driver");
			var jdbcUrl      = context.getInitParameter("jdbc-url");
			var jdbcUsername = context.getInitParameter("jdbc-username");
			var jdbcPassword = context.getInitParameter("jdbc-password");
			/*
			var poolMaxSize     = Integer.parseInt(context.getInitParameter("pool-max-size"));
			ConnectionPool.getInstance().init(jdbcDriver, jdbcUrl, jdbcUsername, jdbcPassword, poolMaxSize);
			//*/
			ConnectionPool.getInstance().init(jdbcDriver, jdbcUrl, jdbcUsername, jdbcPassword);

			// Registering factory of connections
			IoCContainer.registerFactory(Connection.class, new ConnectionFactory());

			// Registering DAO classes
			BiConsumer<ScopeDaoPostgreSqlImpl, Connection> scopeDaoPostgreSqlConnectionInjector = ScopeDaoPostgreSqlImpl::setConnection;
			IoCContainer.registerClass(ScopeDao.class, ScopeDaoPostgreSqlImpl.class, map(dependency(Connection.class, scopeDaoPostgreSqlConnectionInjector)));

			// Registering service classes
			BiConsumer<ScopeServiceImpl, ScopeDao> scopeServiceScopeDaoInjector = ScopeServiceImpl::setScopeDao;
			IoCContainer.registerClass(ScopeService.class, ScopeServiceImpl.class, map(dependency(ScopeDao.class, scopeServiceScopeDaoInjector)));
		} catch(PoolException | IoCException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		ConnectionPool.getInstance().destroy();
	}

	@SafeVarargs
	private Map<Class<?>, BiConsumer<?, ?>> map(Map.Entry<Class<?>, BiConsumer<?, ?>> ... entries) {
		var result = new HashMap<Class<?>, BiConsumer<?, ?>>();
		for(var entry : entries) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	private Map.Entry<Class<?>, BiConsumer<?, ?>> dependency(Class<?> abstraction, BiConsumer<?, ?> injector) {
		return new AbstractMap.SimpleEntry<>(abstraction, injector);
	}
}
