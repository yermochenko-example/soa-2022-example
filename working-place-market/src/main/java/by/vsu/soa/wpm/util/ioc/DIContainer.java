package by.vsu.soa.wpm.util.ioc;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

class DIContainer {
	private static final Map<Class<?>, Map<Class<?>, BiConsumer<Object, Object>>> dependencyInjectionMap = new HashMap<>();

	private final IoCContainer ioc;

	DIContainer(IoCContainer ioc) {
		this.ioc = ioc;
	}

	void injectDependencies(Object object) throws IoCException {
		var dependencies = dependencyInjectionMap.get(object.getClass());
		if(dependencies != null) {
			try {
				for(var entry : dependencies.entrySet()) {
					var dependency = ioc.get(entry.getKey());
					var injector = entry.getValue();
					injector.accept(object, dependency);
				}
			} catch(IllegalArgumentException e) {
				throw new IoCException(e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	static void registerClass(Class<?> implementation, Map<Class<?>, BiConsumer<?, ?>> dependencies) {
		var actualDependencies = new HashMap<Class<?>, BiConsumer<Object, Object>>();
		dependencyInjectionMap.put(implementation, actualDependencies);
		for(var entry : dependencies.entrySet()) {
			var dependency = entry.getKey();
			var injector = entry.getValue();
			actualDependencies.put(dependency, (BiConsumer<Object, Object>) injector);
		}
	}
}
