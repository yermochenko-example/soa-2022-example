package by.vsu.soa.wpm.dao;

public class DaoException extends Exception {
	public DaoException(Throwable cause) {
		super(cause);
	}
}
