package by.vsu.soa.wpm.util.pool;

public class PoolException extends Exception {
	public PoolException(Throwable cause) {
		super(cause);
	}

	public PoolException(String message) {
		super(message);
	}
}
