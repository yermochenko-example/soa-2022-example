package by.vsu.soa.wpm.web.rest;

import by.vsu.soa.wpm.service.ScopeService;
import by.vsu.soa.wpm.service.ServiceException;
import by.vsu.soa.wpm.util.ioc.IoCContainer;
import by.vsu.soa.wpm.util.ioc.IoCException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/scope")
public class ScopeController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try(var ioc = new IoCContainer()) {
			var scopeService = ioc.get(ScopeService.class);
			var mapper = new ObjectMapper();
			var idParam = req.getParameter("id");
			if(idParam == null) {
				var scopes = scopeService.findAll();
				resp.setStatus(200);
				resp.setContentType("application/json");
				resp.setCharacterEncoding("UTF-8");
				mapper.writeValue(resp.getOutputStream(), scopes);
			} else {
				try {
					var id = Long.valueOf(idParam);
					var scope = scopeService.findById(id);
					if(scope != null) {
						resp.setStatus(200);
						resp.setContentType("application/json");
						resp.setCharacterEncoding("UTF-8");
						mapper.writeValue(resp.getOutputStream(), scope);
					} else {
						throw new IllegalArgumentException();
					}
				} catch(IllegalArgumentException e) {
					resp.sendError(404);
				}
			}
		} catch(IoCException | ServiceException e) {
			throw new ServletException(e);
		}
	}
}
