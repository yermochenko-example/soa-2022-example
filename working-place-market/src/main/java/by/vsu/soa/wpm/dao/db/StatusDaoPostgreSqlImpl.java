package by.vsu.soa.wpm.dao.db;

import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.dao.StatusDao;
import by.vsu.soa.wpm.domain.Status;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StatusDaoPostgreSqlImpl extends BaseDaoPostgreSqlImpl<Status> implements StatusDao {
	@Override
	protected String queryStringForInsert() {
		return "INSERT INTO \"status\" (\"name\", \"active\") VALUES (?, ?)";
	}

	@Override
	protected String queryStringForSelect() {
		return "SELECT \"id\", \"name\", \"active\" FROM \"status\" WHERE \"id\" = ?";
	}

	@Override
	protected String queryStringForUpdate() {
		return "UPDATE \"status\" SET \"name\" = ?, \"active\" = ? WHERE \"id\" = ?";
	}

	@Override
	protected String queryStringForDelete() {
		return "DELETE FROM \"status\" WHERE \"id\" = ?";
	}

	@Override
	protected Status buildEntity(ResultSet rs) throws SQLException {
		var status = new Status();
		status.setId(rs.getLong("id"));
		status.setName(rs.getString("name"));
		status.setActive(rs.getBoolean("active"));
		return status;
	}

	@Override
	protected void fillPreparedStatementForInsert(PreparedStatement ps, Status status) throws SQLException {
		ps.setString(1, status.getName());
		ps.setBoolean(2, status.isActive());
	}

	@Override
	protected void fillPreparedStatementForUpdate(PreparedStatement ps, Status status) throws SQLException {
		fillPreparedStatementForInsert(ps, status);
		ps.setLong(3, status.getId());
	}

	@Override
	public List<Status> readAll() throws DaoException {
		var statuses = new ArrayList<Status>();
		readWithCriteria(
			"SELECT \"id\", \"name\", \"active\" FROM \"status\"",
			null,
			statuses::add
		);
		return statuses;
	}
}
