package by.vsu.soa.wpm.dao.db;

import by.vsu.soa.wpm.dao.ClientDao;
import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.domain.Client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDaoPostgreSqlImpl extends BaseDaoPostgreSqlImpl<Client> implements ClientDao {
	@Override
	protected String queryStringForInsert() {
		return "INSERT INTO \"client\" (\"name\", \"address\", \"phone\") VALUES (?, ?, ?)";
	}

	@Override
	protected String queryStringForSelect() {
		return "SELECT \"id\", \"name\", \"address\", \"phone\" FROM \"client\" WHERE \"id\" = ?";
	}

	@Override
	protected String queryStringForUpdate() {
		return "UPDATE \"client\" SET \"name\" = ?, \"address\" = ?, \"phone\" = ? WHERE \"id\" = ?";
	}

	@Override
	protected String queryStringForDelete() {
		return "DELETE FROM \"client\" WHERE \"id\" = ?";
	}

	@Override
	protected Client buildEntity(ResultSet rs) throws SQLException {
		var client = new Client();
		client.setId(rs.getLong("id"));
		client.setName(rs.getString("name"));
		client.setAddress(rs.getString("address"));
		client.setPhone(rs.getString("phone"));
		return client;
	}

	@Override
	protected void fillPreparedStatementForInsert(PreparedStatement ps, Client client) throws SQLException {
		ps.setString(1, client.getName());
		ps.setString(2, client.getAddress());
		ps.setString(3, client.getPhone());
	}

	@Override
	protected void fillPreparedStatementForUpdate(PreparedStatement ps, Client client) throws SQLException {
		fillPreparedStatementForInsert(ps, client);
		ps.setLong(4, client.getId());
	}

	@Override
	public List<Client> readAll() throws DaoException {
		var clients = new ArrayList<Client>();
		readWithCriteria(
			"SELECT \"id\", \"name\", \"address\", \"phone\" FROM \"client\"",
			null,
			clients::add
		);
		return clients;
	}
}
