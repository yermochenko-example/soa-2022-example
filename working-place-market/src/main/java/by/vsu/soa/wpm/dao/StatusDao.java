package by.vsu.soa.wpm.dao;

import by.vsu.soa.wpm.domain.Status;

import java.util.List;

public interface StatusDao extends Dao<Status> {
	List<Status> readAll() throws DaoException;
}
