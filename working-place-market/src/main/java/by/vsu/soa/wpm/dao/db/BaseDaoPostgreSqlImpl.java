package by.vsu.soa.wpm.dao.db;

import by.vsu.soa.wpm.dao.Dao;
import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.domain.Entity;

import java.sql.*;
import java.util.Objects;

abstract public class BaseDaoPostgreSqlImpl<T extends Entity> implements Dao<T> {
	private Connection connection;

	protected Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	@Override
	public Long create(T entity) throws DaoException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(queryStringForInsert(), Statement.RETURN_GENERATED_KEYS);
			fillPreparedStatementForInsert(ps, entity);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			rs.next();
			return rs.getLong(1);
		} catch(SQLException e) {
			throw new DaoException(e);
		} finally {
			try { Objects.requireNonNull(rs).close(); } catch(Exception e) { e.printStackTrace(); }
			try { Objects.requireNonNull(ps).close(); } catch(Exception e) { e.printStackTrace(); }
		}
	}

	@Override
	public T read(Long id) throws DaoException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(queryStringForSelect());
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if(rs.next()) {
				return buildEntity(rs);
			} else {
				return null;
			}
		} catch(SQLException e) {
			throw new DaoException(e);
		} finally {
			try { Objects.requireNonNull(rs).close(); } catch(Exception e) { e.printStackTrace(); }
			try { Objects.requireNonNull(ps).close(); } catch(Exception e) { e.printStackTrace(); }
		}
	}

	@Override
	public void update(T entity) throws DaoException {
		PreparedStatement ps = null;
		try {
			ps = getConnection().prepareStatement(queryStringForUpdate());
			fillPreparedStatementForUpdate(ps, entity);
			ps.executeUpdate();
		} catch(SQLException e) {
			throw new DaoException(e);
		} finally {
			try { Objects.requireNonNull(ps).close(); } catch(Exception e) { e.printStackTrace(); }
		}
	}

	@Override
	public void delete(Long id) throws DaoException {
		PreparedStatement ps = null;
		try {
			ps = getConnection().prepareStatement(queryStringForDelete());
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch(SQLException e) {
			throw new DaoException(e);
		} finally {
			try { Objects.requireNonNull(ps).close(); } catch(Exception e) { e.printStackTrace(); }
		}
	}

	protected void readWithCriteria(String sql, SearchCriteriaFiller filler, FoundEntityHandler<T> entityHandler) throws DaoException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getConnection().prepareStatement(sql);
			if(filler != null) {
				filler.fillSearchCriteria(ps);
			}
			rs = ps.executeQuery();
			while(rs.next()) {
				entityHandler.processEntity(buildEntity(rs));
			}
		} catch(SQLException e) {
			throw new DaoException(e);
		} finally {
			try { Objects.requireNonNull(rs).close(); } catch(Exception e) { e.printStackTrace(); }
			try { Objects.requireNonNull(ps).close(); } catch(Exception e) { e.printStackTrace(); }
		}
	}

	abstract protected String queryStringForInsert();

	abstract protected String queryStringForSelect();

	abstract protected String queryStringForUpdate();

	abstract protected String queryStringForDelete();

	abstract protected T buildEntity(ResultSet rs) throws SQLException;

	abstract protected void fillPreparedStatementForInsert(PreparedStatement ps, T entity) throws SQLException;

	abstract protected void fillPreparedStatementForUpdate(PreparedStatement ps, T entity) throws SQLException;

	protected interface SearchCriteriaFiller {
		void fillSearchCriteria(PreparedStatement statement) throws SQLException;
	}

	protected interface FoundEntityHandler<E extends Entity> {
		void processEntity(E entity);
	}
}
