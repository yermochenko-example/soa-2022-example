package by.vsu.soa.wpm.util.ioc;

public interface Factory<T> {
	T newInstance() throws IoCException;
}
