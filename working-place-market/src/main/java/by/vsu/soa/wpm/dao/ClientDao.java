package by.vsu.soa.wpm.dao;

import by.vsu.soa.wpm.domain.Client;

import java.util.List;

public interface ClientDao extends Dao<Client> {
	List<Client> readAll() throws DaoException;
}
