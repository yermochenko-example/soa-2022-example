package by.vsu.soa.wpm.util.ioc;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class IoCContainer implements AutoCloseable {
	private static final Map<Class<?>, Class<?>> dependencyInversionMap = new HashMap<>();
	private static final Map<Class<?>, Factory<?>> factories = new HashMap<>();

	private final Map<Class<?>, Object> cache = new HashMap<>();
	private final DIContainer dic = new DIContainer(this);

	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> key) throws IoCException {
		var object = (T) cache.get(key);
		if(object == null) {
			try {
				var value = dependencyInversionMap.get(key);
				if(value != null) {
					object = (T) value.getConstructor().newInstance();
				} else {
					var factory = factories.get(key);
					if(factory != null) {
						object = (T) factory.newInstance();
					}
				}
				if(object != null) {
					cache.put(key, object);
					dic.injectDependencies(object);
				}
			} catch(NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
				throw new IoCException(e);
			}
		}
		return object;
	}

	public static void registerClass(Class<?> abstraction, Class<?> implementation, Map<Class<?>, BiConsumer<?, ?>> dependencies) throws IoCException {
		dependencyInversionMap.put(abstraction, implementation);
		if(dependencies != null && !dependencies.isEmpty()) {
			DIContainer.registerClass(implementation, dependencies);
		}
	}

	public static void registerClass(Class<?> abstraction, Class<?> implementation) throws IoCException {
		registerClass(abstraction, implementation, null);
	}

	public static void registerFactory(Class<?> abstraction, Factory<?> factory) {
		factories.put(abstraction, factory);
	}

	@Override
	public void close() {
		for(var object : cache.values()) {
			if(object instanceof AutoCloseable) {
				try { ((AutoCloseable) object).close(); } catch(Exception e) { e.printStackTrace(); }
			}
		}
	}
}
