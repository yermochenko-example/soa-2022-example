package by.vsu.soa.wpm.dao;

import by.vsu.soa.wpm.domain.Order;

import java.util.List;

public interface OrderDao extends Dao<Order> {
	List<Order> readByClient(Long clientId) throws DaoException;
}
