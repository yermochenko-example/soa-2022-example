package by.vsu.soa.wpm.dao.db;

import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.dao.ScopeDao;
import by.vsu.soa.wpm.domain.Scope;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ScopeDaoPostgreSqlImpl extends BaseDaoPostgreSqlImpl<Scope> implements ScopeDao {
	@Override
	protected String queryStringForInsert() {
		return "INSERT INTO \"scope\" (\"name\") VALUES (?)";
	}

	@Override
	protected String queryStringForSelect() {
		return "SELECT \"id\", \"name\" FROM \"scope\" WHERE \"id\" = ?";
	}

	@Override
	protected String queryStringForUpdate() {
		return "UPDATE \"scope\" SET \"name\" = ? WHERE \"id\" = ?";
	}

	@Override
	protected String queryStringForDelete() {
		return "DELETE FROM \"scope\" WHERE \"id\" = ?";
	}

	@Override
	protected Scope buildEntity(ResultSet rs) throws SQLException {
		var scope = new Scope();
		scope.setId(rs.getLong("id"));
		scope.setName(rs.getString("name"));
		return scope;
	}

	@Override
	protected void fillPreparedStatementForInsert(PreparedStatement ps, Scope scope) throws SQLException {
		ps.setString(1, scope.getName());
	}

	@Override
	protected void fillPreparedStatementForUpdate(PreparedStatement ps, Scope scope) throws SQLException {
		fillPreparedStatementForInsert(ps, scope);
		ps.setLong(2, scope.getId());
	}

	@Override
	public List<Scope> readAll() throws DaoException {
		var scopes = new ArrayList<Scope>();
		readWithCriteria(
			"SELECT \"id\", \"name\" FROM \"scope\"",
			null,
			scopes::add
		);
		return scopes;
	}
}
