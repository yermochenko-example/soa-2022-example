package by.vsu.soa.wpm.dao;

import by.vsu.soa.wpm.domain.Scope;

import java.util.List;

public interface ScopeDao extends Dao<Scope> {
	List<Scope> readAll() throws DaoException;
}
