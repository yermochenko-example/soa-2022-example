package by.vsu.soa.wpm.dao.db;

import by.vsu.soa.wpm.dao.DaoException;
import by.vsu.soa.wpm.dao.OrderDao;
import by.vsu.soa.wpm.domain.Client;
import by.vsu.soa.wpm.domain.Order;
import by.vsu.soa.wpm.domain.Scope;
import by.vsu.soa.wpm.domain.Status;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

public class OrderDaoPostgreSqlImpl extends BaseDaoPostgreSqlImpl<Order> implements OrderDao {
	@Override
	protected String queryStringForInsert() {
		return "INSERT INTO \"order\" (\"client_id\", \"hardware\", \"software\", \"amount\", \"price\", \"status_id\", \"date_open\", \"date_close\") VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	}

	@Override
	protected String queryStringForSelect() {
		return "SELECT \"id\", \"client_id\", \"hardware\", \"software\", \"amount\", \"price\", \"status_id\", \"date_open\", \"date_close\" FROM \"order\" WHERE \"id\" = ?";
	}

	@Override
	protected String queryStringForUpdate() {
		return "UPDATE \"order\" SET \"client_id\" = ?, \"hardware\" = ?, \"software\" = ?, \"amount\" = ?, \"price\" = ?, \"status_id\" = ?, \"date_open\" = ?, \"date_close\" = ? WHERE \"id\" = ?";
	}

	@Override
	protected String queryStringForDelete() {
		return "DELETE FROM \"order\" WHERE \"id\" = ?";
	}

	@Override
	protected Order buildEntity(ResultSet rs) throws SQLException {
		var order = new Order();
		order.setId(rs.getLong("id"));
		order.setClient(new Client());
		order.getClient().setId(rs.getLong("client_id"));
		order.setHardware(rs.getString("hardware"));
		order.setSoftware(rs.getString("software"));
		order.setAmount(rs.getInt("amount"));
		order.setPrice(rs.getInt("price"));
		order.setStatus(new Status());
		order.getStatus().setId(rs.getLong("status_id"));
		order.setDateOpen(new java.util.Date(rs.getDate("date_open").getTime()));
		var dateClose = rs.getDate("date_close");
		if(!rs.wasNull()) {
			order.setDateClose(new java.util.Date(dateClose.getTime()));
		}
		return order;
	}

	@Override
	protected void fillPreparedStatementForInsert(PreparedStatement ps, Order order) throws SQLException {
		ps.setLong(1, order.getClient().getId());
		ps.setString(2, order.getHardware());
		ps.setString(3, order.getSoftware());
		ps.setInt(4, order.getAmount());
		ps.setInt(5, order.getPrice());
		ps.setLong(6, order.getStatus().getId());
		ps.setDate(7, new java.sql.Date(order.getDateOpen().getTime()));
		var dateClose = order.getDateClose();
		if(dateClose != null) {
			ps.setDate(8, new java.sql.Date(dateClose.getTime()));
		} else {
			ps.setNull(8, Types.DATE);
		}
	}

	@Override
	protected void fillPreparedStatementForUpdate(PreparedStatement ps, Order order) throws SQLException {
		fillPreparedStatementForInsert(ps, order);
		ps.setLong(9, order.getId());
	}

	@Override
	public Long create(Order order) throws DaoException {
		Long id = super.create(order);
		if(order.getScopes() != null && !order.getScopes().isEmpty()) {
			insertScopes(id, order.getScopes());
		}
		return id;
	}

	@Override
	public Order read(Long id) throws DaoException {
		var order = super.read(id);
		readScopes(order);
		return order;
	}

	@Override
	public List<Order> readByClient(Long clientId) throws DaoException {
		var orders = new ArrayList<Order>();
		readWithCriteria(
			"SELECT \"id\", \"client_id\", \"hardware\", \"software\", \"amount\", \"price\", \"status_id\", \"date_open\", \"date_close\" FROM \"order\" WHERE \"client_id\" = ?",
			ps -> ps.setLong(1, clientId),
			orders::add
		);
		for(var order : orders) {
			readScopes(order);
		}
		return orders;
	}

	@Override
	public void update(Order order) throws DaoException {
		super.update(order);
		deleteScopes(order.getId());
		insertScopes(order.getId(), order.getScopes());
	}

	private void insertScopes(Long orderId, Set<Scope> scopes) throws DaoException {
		var sql = "INSERT INTO \"order_scopes\" (\"order_id\", \"scope_id\") VALUES (?, ?)";
		PreparedStatement ps = null;
		try {
			ps = getConnection().prepareStatement(sql);
			for(var scope : scopes) {
				ps.setLong(1, orderId);
				ps.setLong(2, scope.getId());
				ps.executeUpdate();
			}
		} catch(SQLException e) {
			throw new DaoException(e);
		} finally {
			try { Objects.requireNonNull(ps).close(); } catch(Exception e) { e.printStackTrace(); }
		}
	}

	private void readScopes(Order order) throws DaoException {
		var sql = "SELECT \"scope_id\" FROM \"order_scopes\" WHERE \"order_id\" = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getConnection().prepareStatement(sql);
			ps.setLong(1, order.getId());
			rs = ps.executeQuery();
			order.setScopes(new HashSet<>());
			while(rs.next()) {
				var scope = new Scope();
				scope.setId(rs.getLong("scope_id"));
				order.getScopes().add(scope);
			}
		} catch(SQLException e) {
			throw new DaoException(e);
		} finally {
			try { Objects.requireNonNull(rs).close(); } catch(Exception e) { e.printStackTrace(); }
			try { Objects.requireNonNull(ps).close(); } catch(Exception e) { e.printStackTrace(); }
		}
	}

	private void deleteScopes(Long orderId) throws DaoException {
		var sql = "DELETE FROM \"order_scopes\" WHERE \"order_id\" = ?";
		PreparedStatement ps = null;
		try {
			ps = getConnection().prepareStatement(sql);
			ps.setLong(1, orderId);
			ps.executeUpdate();
		} catch(SQLException e) {
			throw new DaoException(e);
		} finally {
			try { Objects.requireNonNull(ps).close(); } catch(Exception e) { e.printStackTrace(); }
		}
	}
}
