package by.vsu.soa.wpm.domain;

public class Scope extends Entity {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
