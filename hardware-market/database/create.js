// run in console using:
// mongo soa_2022_hardware_market_db <full path>\create.js
db.dropDatabase();
db.computers.insert([
	{
		"name": "Скиф 987",
		"price": 278000,
		"currency": "BYN",
		"CPU-cores": 2,
		"CPU-core-speed": 3.1,
		"RAM": 8,
		"HDD-volume": 2048
	}, {
		"name": "HP 1234",
		"price": 154800,
		"currency": "USD",
		"CPU-cores": 4,
		"CPU-core-speed": 2.3,
		"RAM": 16,
		"HDD-volume": 2048
	}, {
		"name": "Intel 56",
		"price": 231400,
		"currency": "USD",
		"CPU-cores": 6,
		"CPU-core-speed": 2.2,
		"RAM": 32,
		"HDD-volume": 4096
	}, {
		"name": "Compaq 7-89",
		"price": 178500,
		"currency": "EUR",
		"CPU-cores": 4,
		"CPU-core-speed": 2.8,
		"RAM": 32,
		"HDD-volume": 3072
	}, {
		"name": "Скиф 012",
		"price": 734000,
		"currency": "BYN",
		"CPU-cores": 8,
		"CPU-core-speed": 2.4,
		"RAM": 64,
		"HDD-volume": 4096
	}, {
		"name": "Compaq 3-45",
		"price": 321300,
		"currency": "EUR",
		"CPU-cores": 8,
		"CPU-core_speed": 2.9,
		"RAM": 64,
		"HDD-volume": 6144
	}
]);
