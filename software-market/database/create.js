// run in console using:
// mongo soa_2022_software_market_db <full path>\create.js
db.dropDatabase();
db.applications.insert([
	{
		"name": "3Ds Max",
		"price": 3750000,
		"currency": "BYN",
		"scope": ["3D-графика"],
		"CPU-speed": 3.0,
		"RAM": 10.0,
		"HDD-volume": 100
	}, {
		"name": "Adobe Acrobat Reader",
		"price": 0,
		"currency": "USD",
		"scope": ["Обработка документов", "2D-графика"],
		"CPU-speed": 0.5,
		"RAM": 0.5
	}, {
		"name": "Adobe Photoshop",
		"price": 295000,
		"currency": "USD",
		"scope": ["2D-графика"],
		"CPU-speed": 2.0,
		"RAM": 3.5,
		"HDD-volume": 50
	}, {
		"name": "Adobe Premiere Pro",
		"price": 592500,
		"currency": "USD",
		"scope": ["Монтаж видео"],
		"CPU-speed": 10.0,
		"RAM": 35.0,
		"HDD-volume": 2000
	}, {
		"name": "Auto CAD",
		"price": 4250000,
		"currency": "BYN",
		"scope": ["3D-графика"],
		"CPU-speed": 1.0,
		"RAM": 8.0,
		"HDD-volume": 50
	}, {
		"name": "Corel Draw",
		"price": 165000,
		"currency": "EUR",
		"scope": ["2D-графика"],
		"CPU-speed": 1.5,
		"RAM": 2.5,
		"HDD-volume": 25
	}, {
		"name": "Google Chrome",
		"price": 0,
		"currency": "USD",
		"scope": ["Интернет-сёрфинг", "Обработка документов"],
		"CPU-speed": 3.5,
		"RAM": 5.0,
		"HDD-volume": 15
	}, {
		"name": "Microsoft Office",
		"price": 35000,
		"currency": "USD",
		"scope": ["Обработка документов"],
		"CPU-speed": 1.5,
		"RAM": 1.5,
		"HDD-volume": 10
	}
]);
